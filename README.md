hello-docker

https://www.udemy.com/docker-for-beginners

# Docker keep images/containers here :

/Users/gsam/Library/Containers/com.docker.docker

---

# List of Commands

## 1. Understanding FROM and RUN

```bash
docker build -t demo_tree .
docker run -it --rm demo_tree
```

-it : interactive mode
exit : to quit

## 2. Understanding CMD

```bash
docker build -t command_docker .
docker run --rm command_docker
```

## 3. Understanding COPY

```bash
docker build -t add_docker .
docker run -it --rm add_docker
```

_fi closes the if statement, while ;; closes the current entry in the case statement._

Supprimer un container :

```
docker ps -a
    CONTAINER ID        IMAGE     ...
    2b3d67d0e84c        demo_tree ...
docker rm 2b3d67d0e84c
```

Pour supprimer toutes les données inutilisées:

```
$ docker system df
$ docker system prune
```

### 4. Understanding ADD

### 5. Understanding WORKDIR

### 6. Understanding ENTRYPOINT

ENTRYPOINT sert à configurer un container au démarrage,
CMD est utilisé pour définir la commande de démarrage par défaut du conteneur.

### 7. Understanding ENV

### 8. Understanding LABEL

```
docker build --label "key"="value" -t label_docker .
```

### 8. Understanding HEALTHCHECK

```
docker build -t health_docker .
docker run -it --name health_docker --rm health_docker
```

nouvel onglet

```
docker inspect health_docker
```

### 10. Understanding STOPSIGNAL

```
docker build -t stop_docker .
docker run -it --name stop_docker --rm stop_docker
```

nouvel onglet

```
docker stop stop_docker
```

### 11. Understanding VOLUMES

```
docker run -it --name busybox_1 -v /data busybox
docker run -it --name busybox_2 busybox
docker rm busybox_2
docker run -it --name busybox_2 --volumes-from busybox_1 busybox
```

```
MacBookPro-Gsam:~ gsam$ docker system df
TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              7                   1                   91.29MB             91.29MB (100%)
Containers          2                   0                   44B                 44B (100%)
Local Volumes       1                   1                   0B                  0B
Build Cache         0                   0                   0B                  0B
MacBookPro-Gsam:~ gsam$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS                          PORTS               NAMES
dd1232cbcc3b        busybox             "/bin/bash"         About a minute ago   Exited (0) About a minute ago                       busybox_2
60c24461adc6        busybox             "/bin/bash"         2 minutes ago        Exited (0) About a minute ago                       busybox_1
```

### 12. Understanding MOUNT

```
docker run -it -v "$(pwd)/Flask_apps":/Flask_apps ubuntu
```

### 13. Understanding EXPOSE

```
docker build -t expose_docker .
docker run --rm -p 8080:5000 expose_docker
```

Erreur au build :

```
Digest: sha256:76df62c122c910751d8cd3101f8e3da39efd4ee828686b7ff0b5a5b1d967553f
Status: Downloaded newer image for python:3-onbuild
# Executing 3 build triggers
COPY failed: stat /var/lib/docker/tmp/docker-builder897315505/requirements.txt: no such file or directory
MacBookPro-Gsam:hello-docker gsam$ docker run --rm -p 8080:5000 expose_docker
Unable to find image 'expose_docker:latest' locally
docker: Error response from daemon: pull access denied for expose_docker, repository does not exist or may require 'docker login': denied: requested access to the resource is denied.
See 'docker run --help'.
```

#### Commandes docker

_Chercher une image dans un dépôt_

```
docker search lamp
...
docker pull linode/lamp
docker build .
```

_Lister les images_

```
docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
<none>              <none>              64592ce9857c        24 seconds ago      64.2MB

docker tag 64592ce9857c mkf

MacBookPro-Gsam:hello-docker gsam$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mkf                 latest              64592ce9857c        2 minutes ago       64.2MB
```

_Démarrer un conteneur_

```
docker start identifiantImage
docker run mkf:latest
```

_Éteindre un conteneur_

```
docker stop identifiantImage
docker pause identifiantImage

```

_Se connecter à votre conteneur_

```
docker ps
docker exec -it identifiantImage bash

```

_Conserver les modifications « éphémères » d'un conteneur_

```
docker commit identifiantImage monBackup
```

_Sauvegarde d'un conteneur dans une archive_

```
docker save identifiantImage > maSauvegarde.tar
```

_Restauration d'un conteneur depuis une archive_

```
docker load maSauvegarde.tar
```

### 14. Understanding Docker Container Networking Part 1

```
docker network create --subnet 172.28.0.0/16 GsamNetwork
```

## Monter un serveur LAMP grâce à Docker https://doc.ubuntu-fr.org/docker_lamp

### Méthode simple

```
mkdir -p www ~/.docker/mysql
chmod 777 www ~/.docker/mysql
docker run -v ~/.docker/www:/var/www/html -v ~/.docker/mysql:/var/lib/mysql -p 80:80 -p 3306:3306 --restart=always lioshi/lamp:php5
```

### Méthode avancée

_docker-compose_

Outil qui permet de lancer plusieurs conteneurs en évitant les lignes de commandes à rallonge.
Créer un fichier docker-compose.yml

```yml
version: '2'

services:
    web:
        image: lavoweb/php-5.6
        ports:
            - '80:80'
        volumes:
            - ~/www:/var/www/html
        links:
            - db:db
    db:
        image: mysql:5.5
        volumes:
            - ~/mysql:/var/lib/mysql
        ports:
            - '3306:3306'
        environment:
            - MYSQL_ROOT_PASSWORD=root
    myadmin:
        image: phpmyadmin/phpmyadmin
        ports:
            - '8080:80'
        links:
            - db:db
```

```
docker-compose up
```
